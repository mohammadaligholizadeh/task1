<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SimpleController;
use App\Http\Controllers\createController;
use App\Http\Controllers\updateController;
use App\Http\Controllers\V1\ProductController;
use App\Http\Controllers\V1\VendorController;

Route::get('simple',[SimpleController::class,'test']);
Route::post('create',[createController::class,'create_product']);
Route::post('update',[updateController::class,'update_product']);


Route::post('products/v1',[ProductController::class,'create']);
Route::post('products/v1/{id}',[ProductController::class,'update']);
Route::post('product/v1/{product_id}',[productController::class,'fetchProduct']);

Route::post('vendors/v1',[VendorController::class,'create']);
Route::post('vendors/v1/{id}',[VendorController::class,'update']);
Route::post('vendors/v1/{vendor_id}/import-products',[VendorController::class,'importProducts']);
Route::post('vendors/v1/{vendorId}/products',[VendorController::class,'fetchSortedProducts']);


