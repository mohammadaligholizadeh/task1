<?php

namespace App\Const;

class RedisConstant
{
    const CACHE_PRODUCT_PREFIX = 'PRODUCT:';
    const CACHE_VENDOR_PREFIX = 'VENDOR:';
    const CACHE_VENDOR_TOP_PRODUCT_PREFIX = 'VENDOR_TOP:';

    const SHOW_PRODUCT_BY_ID_EXPIRE_TIME = 60;
}
