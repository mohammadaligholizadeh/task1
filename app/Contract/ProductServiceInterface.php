<?php

namespace App\Contract;

use App\Dto\ProductDto;

interface ProductServiceInterface
{
    public function create(ProductDto $productDto): void;

    public function update(ProductDto $productDto, int $id): void;

}
