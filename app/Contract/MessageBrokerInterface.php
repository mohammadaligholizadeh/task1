<?php

namespace App\Contract;

interface MessageBrokerInterface
{
    public function queue(string $name);

    public function channel();
}
