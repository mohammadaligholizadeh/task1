<?php

namespace App\Contract;

use App\Dto\VendorDto;
use App\Dto\VendorProductCollectionDto;

interface VendorServiceInterface
{

    public function create(VendorDto $vendorDto): void;

    public function update(VendorDto $vendorDto, int $id): void;

    public function importProducts(VendorProductCollectionDto $vendorProductCollectionDto, int $vendorId): void;

}
