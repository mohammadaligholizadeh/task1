<?php

namespace App\Contract;

use PhpAmqpLib\Connection\AMQPStreamConnection;

abstract class AbstractRabbitmqBroker implements MessageBrokerInterface
{

    protected $connection;
    protected $channel;

    public function __construct()
    {
        $this->connection = new AMQPStreamConnection(
            app('config')->get('queue.connections.rabbitmq.host'),
            app('config')->get('queue.connections.rabbitmq.port'),
            app('config')->get('queue.connections.rabbitmq.username'),
            app('config')->get('queue.connections.rabbitmq.password')
        );
    }

    public function channel()
    {
        $this->channel = $this->connection->channel();
    }

    public function queue(string $name)
    {
        $this->channel->queue_declare($name, false, false, false, false);
    }

}
