<?php

namespace App\Console\Commands;

use App\Contract\AbstractRabbitmqBroker;
use App\Models\VendorProducts;
use Illuminate\Console\Command;
use PhpAmqpLib\Connection\AMQPStreamConnection;

class VendorProductImportCommand extends Command
{

    public function __construct(public AbstractRabbitmqBroker $rabbitmqPublisherAdapter)
    {
        parent::__construct();


    }
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:vendor-product-import-command';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {

        $this->rabbitmqPublisherAdapter->consume();

    }
}
