<?php

namespace App\Proxy;

use App\Const\RedisConstant;
use App\Models\Product;
use App\Services\ProductRepository;
use Illuminate\Support\Facades\Cache;

class CacheProductRepository
{
    public function __construct(
        protected ProductRepository $productRepository
    ) {
    }

    public function getProduct(int $id)
    {
        if ($productCacheValue = Cache::get(RedisConstant::CACHE_PRODUCT_PREFIX.$id)) {

            $product = unserialize($productCacheValue);

        } else {

            $product = $this->productRepository->getProduct($id);
            Cache::set(
                RedisConstant::CACHE_PRODUCT_PREFIX.$id,
                serialize($product),
                RedisConstant::SHOW_PRODUCT_BY_ID_EXPIRE_TIME
            );

        }

        return $product;
    }

    public function getVendorProduct(int $id)
    {
        if ($vendorsCacheValue = Cache::get(RedisConstant::CACHE_VENDOR_PREFIX.$id)) {

            $vendors = unserialize($vendorsCacheValue);

        } else {

            $vendors = $this->productRepository->getVendorProduct($id);
        Cache::set(
                RedisConstant::CACHE_VENDOR_PREFIX.$id,
                serialize($vendors),
                RedisConstant::SHOW_PRODUCT_BY_ID_EXPIRE_TIME
            );

        }

        return $vendors;
    }
}
