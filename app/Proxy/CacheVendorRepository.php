<?php

namespace App\Proxy;

use App\Const\RedisConstant;
use App\Services\VendorRepository;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;

class CacheVendorRepository
{

    public function __construct(
        protected VendorRepository $vendorRepository
    )
    {

    }

    public function topProducts(string $sort, int $id): Collection
    {
        if($items = Cache::get(RedisConstant::CACHE_VENDOR_TOP_PRODUCT_PREFIX.$sort.':'.$id))
        {

            $products = unserialize($items);

        } else {

            $products = $this->vendorRepository->fetchSortedProducts($sort, $id);
            Cache::set(
                RedisConstant::CACHE_VENDOR_TOP_PRODUCT_PREFIX.$sort.':'.$id,
                serialize($products),
                RedisConstant::SHOW_PRODUCT_BY_ID_EXPIRE_TIME
            );

        }

        return $products;
    }

}
