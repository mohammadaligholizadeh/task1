<?php

namespace App\Services;

use App\Models\Product;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class ProductRepository
{
    public function getProduct(int $id)
    {
        return $this->fetchProductDetail($id);
    }

    public function getVendorProduct(int $id)
    {
        return $this->fetchVendorsList($id);
    }

    private function fetchProductDetail(int $id): Product
    {
        return Product::where('id', $id)->first();

        // @todo check find product: exception
    }

    private function fetchVendorsList(int $productId): Collection
    {
        return DB::table('vendors')
            ->join('vendor_product', 'vendors.id', '=', 'vendor_product.vendor_id')
            ->where('vendor_product.product_id', $productId)
            ->select('vendors.*', 'vendor_product.price')
            ->get();

    }
}
