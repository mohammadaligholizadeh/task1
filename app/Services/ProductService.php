<?php

namespace App\Services;

use App\Const\RedisConstant;
use App\Contract\ProductServiceInterface;
use App\Dto\PresentProductDto;
use App\Dto\PresentProductVendorDto;
use App\Dto\ProductDto;
use App\Models\Product;
use App\Models\Vendor;
use App\Proxy\CacheProductRepository;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;

class ProductService implements ProductServiceInterface
{

    public function __construct(protected CacheProductRepository $cacheProductRepository)
    {

    }

    public function getProductDto(array $data): ProductDto
    {
        return new ProductDto(
            title: $data['title'],
            description: $data['description'],
            active: $data['active']
        );
    }

    public function create(ProductDto $productDto): void
    {
        Product::insert($productDto->toArray());
    }

    public function update(ProductDto $productDto, int $id): void
    {
        $product = Product::find($id);
        $product->title = $productDto->title;
        $product->description = $productDto->description;
        $product->active = $productDto->active;

        $product->save();
    }

    public function getVendorProducts(int $productId)
    {

            $product = $this->cacheProductRepository->getProduct($productId);
            $vendors = $this->cacheProductRepository->getVendorProduct($productId);

//            dd($vendors);

            return $this->getPresentProductDto($product, $vendors);

    }

    public function getPresentProductDto(Product $product, Collection $vendors): PresentProductDto
    {

//        dd($vendors);
        $vendorsList = []; // camel case
        /** @var Vendor $vendor */
        foreach ($vendors->toArray() as $vendor) {
            $vendorsList[] = new PresentProductVendorDto(
                id: $vendor->id,
                title: $vendor->title,
                price: $vendor->price,
            );
        }

        return new PresentProductDto(
            id:   $product->id,
            title: $product->title,
            description: $product->description,
            vendors: $vendorsList
        );
    }

}
