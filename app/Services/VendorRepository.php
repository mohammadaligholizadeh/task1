<?php

namespace App\Services;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use App\Models\Vendor;

class VendorRepository
{

    public function fetchSortedProducts(string $sortKey,int $vendorId): Collection
    {
        return DB::table('products')
            ->join('vendor_product','vendor_product.product_id','=','products.id')
            ->join('vendors','vendors.id','=','vendor_product.vendor_id')
            ->where('vendors.id',$vendorId)
            ->select(
                'vendors.title as vendor_name',
                'products.title as product_name',
                'products.description',
                'vendor_product.price as price'
            )
            ->orderBy('price',$sortKey)
            ->take(10)
            ->get();
    }

}
