<?php

namespace App\Services;

use App\Contract\AbstractRabbitmqBroker;
use App\Models\Vendor;
use App\Models\VendorProducts;
use App\Contract\VendorServiceInterface;
use App\Dto\VendorDto;
use App\Dto\VendorProductsDto;
use App\Dto\VendorProductCollectionDto;
use App\Proxy\CacheVendorRepository;
use Illuminate\Support\Collection;

class VendorService implements VendorServiceInterface
{

    const CHUNK_SIZE = 100 ;

    public function __construct(
        public AbstractRabbitmqBroker $rabbitmqPublisherAdapter,
        protected CacheVendorRepository $cacheVendorRepository
    )
    {

    }

    public function getVendorDto(array $data): VendorDto
    {
        return new VendorDto(
          title: $data['title'],
          active: $data['active']
        );
    }

    public function getVendorProductsDto(array $items): VendorProductCollectionDto
    {

        $products = [];
        foreach ($items as $data){

            $products[] = new VendorProductsDto(
                productId: $data['product_id'],
                price: $data['price']
            );

        }

        return new VendorProductCollectionDto(
            products : $products
        );


    }

    public function create(VendorDto $vendorDto): void
    {
        Vendor::insert($vendorDto->toArray());
    }

    public function update(VendorDto $vendorDto, int $id): void
    {
        $vendor = Vendor::find($id);
        $vendor->title = $vendorDto->title;
        $vendor->active = $vendorDto->active;

        $vendor->save();
    }

    public function importProducts(VendorProductCollectionDto $vendorProductCollectionDto, int $vendorId): void
    {



        $results = array_chunk($vendorProductCollectionDto->products,self::CHUNK_SIZE);

        foreach ($results as $items){
            $products = [];
            /** @var VendorProductsDto $vendorProductDto */

            foreach ($items as $key=>$vendorProductDto){

                $products[] = [
                    'product_id' => $vendorProductDto->productId,
                    'price' => $vendorProductDto->price,
                    'vendor_id' => $vendorId
                ];
            }

            $this->rabbitmqPublisherAdapter->publish($products);
        }
    }

    public function insertProducts(array $products): void{
        VendorProducts::upsert($products,['product_id', 'vendor_id'],['price']);
    }

    public function fetchSortedProducts(string $sorted,int $vendorId): Collection
    {
        if($sorted == 'cheapest')
        {

            $directionSort = 'asc';

        } elseif ($sorted == 'expensive')
        {

            $directionSort = 'desc';

        }

        return $this->cacheVendorRepository->topProducts($directionSort,$vendorId);
    }

}
