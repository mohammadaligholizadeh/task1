<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ProductService;

class updateController extends Controller
{
    public function update_product(Request $request){
        $id = $request->input('id');
        $brand = $request->input('brand');
        $model = $request->input('model');

        $test = new ProductService($brand,$model,$id);
        $out = $test->update_product();

    }
}
