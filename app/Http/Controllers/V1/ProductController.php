<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Services\ProductService;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function __construct(
        protected ProductService $productService
    ) {

    }

    public function create(Request $request) // @todo use custom Request for validation
    {
        // request include title, description and active per product
        $productDto = $this->productService->getProductDto($request->all());
        $this->productService->create($productDto);

        return $this->success();
    }

    public function update(Request $request, int $id) // @todo use custom Request for validation
    {
        // request include title, description and active per product
        $productDto = $this->productService->getProductDto($request->all());
        $this->productService->update($productDto, $id);

        return $this->success();
    }

    public function fetchProduct(int $productId)
    {

        $productDto = $this->productService->getVendorProducts($productId);

        return $this->success($productDto->toArray());
    }

}
