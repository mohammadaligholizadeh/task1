<?php

namespace app\Http\Controllers\V1;

use App\Services\VendorService;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class VendorController extends Controller
{

    public function __construct(
        protected VendorService $vendorService
    ){

    }

    public function create(Request $request)
    {

        $vendorDto = $this->vendorService->getVendorDto($request->all());
        $this->vendorService->create($vendorDto);

        return $this->success();

    }

    public function update(Request $request, int $id)
    {

        $vendorDto = $this->vendorService->getVendorDto($request->all());
        $this->vendorService->update($vendorDto, $id);

        return $this->success();

    }

    public function importProducts(Request $request, int $vendorId)
    {

            $vendorProductCollectionDto = $this->vendorService->getVendorProductsDto($request->all());
            $this->vendorService->importProducts($vendorProductCollectionDto,$vendorId);

            return $this->success();

    }

    public function fetchSortedProducts(Request $request, int $vendorId)
    {
//        dd($request['sort']);
        $tets = $this->vendorService->fetchSortedProducts($request['sort'], $vendorId);
        dd($tets);
    }

}
