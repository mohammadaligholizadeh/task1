<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;

    public function success(array $data = [], int $status = 200)
    {
        return response()->json(
            [
                'success' => true,
                'data' => $data,
            ],
            $status
        );
    }

    public function error(array $data = [], int $status = 400)
    {
        return response()->json($data, $status);
    }
}
