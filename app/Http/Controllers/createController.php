<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ProductService;

class createController extends Controller
{
    public function create_product (Request $request) {

        $brand = $request->input('brand');
        $model = $request->input('model');

        $test = new ProductService($brand,$model);
        $out = $test->create_product();
        
    }
}
