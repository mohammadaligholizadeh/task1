<?php

namespace App\Adapter;

use App\Contract\AbstractRabbitmqBroker;
use App\Services\VendorService;
use PhpAmqpLib\Message\AMQPMessage;

class RabbitmqAdapter extends AbstractRabbitmqBroker
{

    const QUEUE_NAME = 'importation';

    public function publish(array $message): void
    {
        $this->channel();
        $this->queue(self::QUEUE_NAME);

        $msg = new AMQPMessage(json_encode($message));
        $this->channel->basic_publish($msg, '', self::QUEUE_NAME);

        $this->channel->close();
        $this->connection->close();

    }

    public function consume(): void
    {
        $this->channel();
        $this->queue(self::QUEUE_NAME);

        $callback = function ($msg) {
            $products = json_decode($msg->getBody(),true);
            VendorService::insertProducts($products);
        };

        $this->channel->basic_consume(self::QUEUE_NAME, '', false, true, false, false, $callback);

        try {
            $this->channel->consume();
        } catch (\Throwable $exception) {
            echo $exception->getMessage();
        }

        $this->channel->close();
        $this->connection->close();

    }

}
