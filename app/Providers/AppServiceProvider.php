<?php

namespace App\Providers;

use App\Adapter\RabbitmqAdapter;
use App\Contract\AbstractRabbitmqBroker;
use App\Contract\ProductServiceInterface;
use App\Services\ProductService;
use App\Contract\VendorServiceInterface;
use App\Services\VendorService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(ProductServiceInterface::class, ProductService::class);
        $this->app->bind(VendorServiceInterface::class, VendorService::class);
        $this->app->bind(AbstractRabbitmqBroker::class, RabbitmqAdapter::class);
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
