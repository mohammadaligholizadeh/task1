<?php

namespace App\Dto;

use Spatie\LaravelData\Data;
class VendorDto extends Data
{

    public function __construct(
        public string $title,
        public bool $active
    )
    {

    }

}
