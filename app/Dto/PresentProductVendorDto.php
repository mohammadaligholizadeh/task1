<?php

namespace App\Dto;

use Spatie\LaravelData\Data;

class PresentProductVendorDto extends Data
{
    public function __construct(
        public int $id,
        public string $title,
        public int $price
    )
    {

    }
}
