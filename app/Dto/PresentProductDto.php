<?php

namespace App\Dto;

use Spatie\LaravelData\Data;

class PresentProductDto extends Data
{
    public function __construct(
        public int $id,
        public string $title,
        public string $description,
        public array $vendors
    )
    {

    }
}
