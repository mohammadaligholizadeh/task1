<?php

namespace App\Dto;

use Spatie\LaravelData\Data;

class ProductDto extends Data
{
    public function __construct(
        public string $title,
        public string $description,
        public bool $active
    ) {

    }
}
