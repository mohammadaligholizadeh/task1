<?php

namespace App\Dto;

use Illuminate\Support\Enumerable;
use Spatie\LaravelData\Data;
class VendorProductCollectionDto extends Data
{

    public function __construct(
        public array $products
    )
    {
    }

}
