<?php

namespace App\Dto;

use Spatie\LaravelData\Data;

class VendorProductsDto extends Data
{

    public function __construct(
        public int $productId,
        public int $price
    )
    {

    }

}
